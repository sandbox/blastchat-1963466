<?php
/**
 * @version	$Id: api_custom.blastchat.php 2013-01-30 15:24:18Z $
 * @package	BlastChat Config
 * @author 	BlastChat
 * @copyright	Copyright (C) 2004-2013 BlastChat. All rights reserved.
 * @license	GNU/GPL, see LICENSE.php
 * @HomePage 	<http://www.blastchat.com>

 * This file is part of BlastChat Config.

 * BlastChat Config is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * BlastChat Config is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with BlastChat Config.  If not, see <http://www.gnu.org/licenses/>.
 */

/** ensure this file is being included by a parent file */

class BlastChatApiCustom extends BlastChatApi
{
	/*
	public function bc_createBlastChatData($bcData, $par) {
		static $called;
		static $bcDataJavascript;
		$output = "";
		$module_path = drupal_get_path('module', 'blastchat');

		$bcData->request = "http://bcs.blastchat.com/bc/dist/bcif/bc.js";

		if ($par) {
			$sid = $par["sid"];
			if (!$sid || empty($sid)) {
				echo "Missing BlastChat Config - Website ID.";
				return;
			}
			$sid = intval($sid);
			$pk = $par["pk"];
			if (empty($pk)) {
				echo "Missing BlastChat Config - Private key.";
				return;
			}
			$guestprefix = $par["guestprefix"];
			if (empty($guestprefix)) {
				echo "Missing BlastChat Config - Guest prefix.";
				return;
			}
		} else {
			echo "Missing BlastChat Config extension.";
			return;
		}
		
		$bcData->sid = $sid;
		$bcData->pk = trim($pk);
		$bcData->guestprefix = trim($guestprefix);
	
		if (!$called) {
			$user = self::bc_getUser();
	
			$bcData->theme = self::bc_getTheme($par);
			
			$bcData->userid = $user->id ? $user->id : 0;
			$bcData->name = $bcData->userid != 0 ? $user->username : uniqid($bcData->guestprefix, false);
			
			$guestnames = self::bc_getGuestNames();
			if (count($guestnames) > 0) {
				$bcData->fullname = $bcData->userid != 0 ? $user->name : $bcData->guestprefix.$guestnames[rand(0, count($guestnames)-1)];
			} else {
				$bcData->fullname = $bcData->userid != 0 ? $user->name : $bcData->name;
			}
			
			$bcData->gids = self::bc_getUserGroups($user);
			$bcData->gender = self::bc_getUserGender($user);
			$bcData->friends = self::bc_getUserFriends($user);
			$bcData->lang = self::bc_getLanguageFile($user, $par);
			
			$bcData->avtPath = "";
			$bcData->avt = "";
			$bcData->avtShow = false;
			if ($par["avtint"] != "0") {
				$avatar = self::bc_getUserAvatar($user, $par["avtint"]);
				$bcData->avtPath = $avatar->commonpath;
				$bcData->avt = $avatar->file;
				$bcData->avtShow = true;
			}
	
			$jversion = self::bc_getVersion();
			$bcData->prod = "'".$jversion->PRODUCT."','".$jversion->RELEASE."','".$jversion->DEV_LEVEL."'";
			
			$bcData->protocol = 0; //0 http, 1 - https (not used)
			$bcData->time = time();
			$bcData->code = hash('sha256', $bcData->time.$bcData->pk.$bcData->userid.$bcData->name.$bcData->gids );
			
			$bcDataJavascript = ""
				."if (typeof bcData === 'undefined') {"
					."var bcData = {"
						."'sid':".$bcData->sid.","
						."'auth':['".$bcData->time."','".$bcData->code."'],"
						."'user': ".json_encode(array(
							'sid' => $bcData->sid,
							'id' => $bcData->userid,
							'name' => $bcData->name,
							'fullname' => $bcData->fullname,
							'gids' => $bcData->gids,
							'gender' => $bcData->gender,
							'avt' => $bcData->avt
							)).","
						."'avtPath':'".$bcData->avtPath."',"
						."'avtShow':".($bcData->avtShow ? 'true' : 'false').","
						."'friends':[".$bcData->friends."],"
						."'bw':".(isset($bcData->barWidth) && $bcData->barWidth > 0 ? $bcData->barWidth : '0').","
						."'bh':".(isset($bcData->barHeight) && $bcData->barHeight > 0 ? $bcData->barHeight : '0').","
						."'detached':".(isset($bcData->detached) ? $bcData->detached : '0').","
						."'protocol':".$bcData->protocol.","
						."'product':[".$bcData->prod."],"
						."'theme':{'name':'".$bcData->theme->name."','cloc':'".$bcData->theme->cloc."','over':'".$bcData->theme->over."'},"
						."'bcc':[]"
					."};"
				."}"
				;
			drupal_add_js($bcDataJavascript, array('type' => 'inline', 'scope' => 'header', 'weight' => 1));
		}
		
		if ($bcData->detached || if ($bcData->interface == 'mob') {
			echo "<link rel=\"stylesheet\" href=\"". $module_path . "/config_default/css/main.css\" type=\"text/css\" />";
			if (is_file($module_path . '/config/css/main.css')) {
				echo "<link rel=\"stylesheet\" href=\"".$module_path . "/config//css/main.css\" type=\"text/css\" />";
			}
			
			echo "\n<link rel=\"stylesheet\" href=\"". $module_path . "/config_default/css/icons.css\" type=\"text/css\" />";
			if (is_file($module_path . '/config/css/icons.css')) {
				echo "\n<link rel=\"stylesheet\" href=\"".$module_path . "/config//css/icons.css\" type=\"text/css\" />";
			}
			
			echo "\n<link rel=\"stylesheet\" href=\"". $module_path . "/config_default/css/emoticons.css\" type=\"text/css\" />";
			if (is_file($module_path . '/config/css/emoticons.css')) {
				echo "\n<link rel=\"stylesheet\" href=\"".$module_path . "/config//css/emoticons.css\" type=\"text/css\" />";
			}
			
			echo "\n<link rel=\"stylesheet\" href=\"". $module_path . "/config_default/css/sounds.css\" type=\"text/css\" />";
			if (is_file($module_path . '/config/css/sounds.css')) {
				echo "\n<link rel=\"stylesheet\" href=\"".$module_path . "/config//css/sounds.css\" type=\"text/css\" />";
			}
			
			if ($bcData->detached) {
				echo "\n<style type=\"text/css\">\nhtml, body, #blastchatchat {top:0; left:0; height: 100%; width: 100%; padding: 0; margin: 0; border: 0; overflow: hidden; position: absolute;}\n#header {margin: 0px;}</style>";
			}
			
			echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/js/loader.js\"></script>";
			
			if (is_file($module_path . '/config/js/parseuri.js')) {
				echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/config/js/parseuri.js\"></script>";
			} else {
				echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/config_default/js/parseuri.js\"></script>";
			}
			if (is_file($module_path . '/config/languages/'. $bcData->lang . '.js')) {
				echo "\n<script type=\"text/javascript\" src=\"".$module_path . "/config/languages/".$bcData->lang.".js\"></script>";
			} else {
				echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/config_default/languages/".$bcData->lang.".js\"></script>";
			}
			if (is_file($module_path . '/config/system.js')) {
				echo "\n<script type=\"text/javascript\" src=\"".$module_path . "/config/system.js\"></script>";
			} else {
				echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/config_default/system.js\"></script>";
			}
			if (is_file($module_path . '/config/rooms.js')) {
				echo "\n<script type=\"text/javascript\" src=\"".$module_path . "/config/rooms.js\"></script>";
			} else {
				echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/config_default/rooms.js\"></script>";
			}
			if (is_file($module_path . '/config/groups.js')) {
				echo "\n<script type=\"text/javascript\" src=\"".$module_path . "/config/groups.js\"></script>";
			} else {
				echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/config_default/groups.js\"></script>";
			}
			if (is_file($module_path . '/config/roomsgroups.js')) {
				echo "\n<script type=\"text/javascript\" src=\"".$module_path . "/config/roomsgroups.js\"></script>";
			} else {
				echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/config_default/roomsgroups.js\"></script>";
			}
			if (is_file($module_path . '/config/emoticons.js')) {
				echo "\n<script type=\"text/javascript\" src=\"".$module_path . "/config/emoticons.js\"></script>";
			} else {
				echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/config_default/emoticons.js\"></script>";
			}
			if (is_file($module_path . '/config/sounds.js')) {
				echo "\n<script type=\"text/javascript\" src=\"".$module_path . "/config/sounds.js\"></script>";
			} else {
				echo "\n<script type=\"text/javascript\" src=\"". $module_path . "/config_default/sounds.js\"></script>";
			}
			echo "\n<script type=\"text/javascript\">";
			echo $bcDataJavascript;
			echo "\nbcData.bcc.push(['".$bcData->id."','".$bcData->client."','".$bcData->interface."',[".$bcData->roomids."], '".$bcData->version."']);";
			echo "\nbcLoader(\"bcJSmain\", \"script\", \"".$bcData->request."\", null, null);";
			echo "\n</script>";
		} else {
			if (!$called) {
				drupal_add_js($module_path .'/js/loader.js');
				if (file_exists($module_path .'config/js/parseuri.js')) {
					drupal_add_js($module_path .'/config/js/parseuri.js');
				} else {
					drupal_add_js($module_path .'/config_default/js/parseuri.js');
				}
				if (file_exists($module_path .'config/languages/'.$bcData->lang.'.js')) {
					drupal_add_js($module_path .'/config/languages/'.$bcData->lang.'.js');
				} else {
					drupal_add_js($module_path .'/config_default/languages/'.$bcData->lang.'.js');
				}
				if (file_exists($module_path .'config/system.js')) {
					drupal_add_js($module_path .'/config/system.js');
				} else {
					drupal_add_js($module_path .'/config_default/system.js');
				}
				if (file_exists($module_path .'config/rooms.js')) {
					drupal_add_js($module_path .'/config/rooms.js');
				} else {
					drupal_add_js($module_path .'/config_default/rooms.js');
				}
				if (file_exists($module_path .'config/groups.js')) {
					drupal_add_js($module_path .'/config/groups.js');
				} else {
					drupal_add_js($module_path .'/config_default/groups.js');
				}
				if (file_exists($module_path .'config/roomsgroups.js')) {
					drupal_add_js($module_path .'/config/roomsgroups.js');
				} else {
					drupal_add_js($module_path .'/config_default/roomsgroups.js');
				}
				if (file_exists($module_path .'config/emoticons.js')) {
					drupal_add_js($module_path .'/config/emoticons.js');
				} else {
					drupal_add_js($module_path .'/config_default/emoticons.js');
				}
				if (file_exists($module_path .'config/sounds.js')) {
					drupal_add_js($module_path .'/config/sounds.js');
				} else {
					drupal_add_js($module_path .'/config_default/sounds.js');
				}

				drupal_add_css($module_path .'/config_default/css/main.css');
				if (file_exists($module_path .'config/css/main.css')) {
					drupal_add_css($module_path .'/config/css/main.css');
				}
				drupal_add_css($module_path .'/config_default/css/icons.css');
				if (file_exists($module_path .'config/css/icons.css')) {
					drupal_add_css($module_path .'/config/css/icons.css');
				}
				drupal_add_css($module_path .'/config_default/css/emoticons.css');
				if (file_exists($module_path .'config/css/emoticons.css')) {
					drupal_add_css($module_path .'/config/css/emoticons.css');
				}
				drupal_add_css($module_path .'/config_default/css/sounds.css');
				if (file_exists($module_path .'config/css/sounds.css')) {
					drupal_add_css($module_path .'/config/css/sounds.css');
				}
				
				drupal_add_js("if (typeof bcData.loaded === 'undefined') {bcLoader('bcJSmain', 'script', '".$bcData->request."', null, null);bcData.loaded = [];}", array('type' => 'inline', 'scope' => 'header', 'weight' => 2));
				//$output .= "<script type=\"text/javascript\">if (typeof bcData.loaded === 'undefined') {bcLoader('bcJSmain', 'script', '".$bcData->request."', null, null);bcData.loaded = [];}</script>\n";

			}
			if (isset($bcData->id)) {
				drupal_add_js("var bcDataObj = ['".$bcData->id."','".$bcData->client."','".$bcData->interface."',[".$bcData->roomids."], '".$bcData->version."'];if (bcData.loaded.indexOf('".$bcData->id."') == -1) {bcData.bcc.push(bcDataObj);bcData.loaded.push('".$bcData->id."');}", array('type' => 'inline', 'scope' => 'header', 'weight' => 3));
				//$output .= "<script type=\"text/javascript\">var bcDataObj = ['".$bcData->id."','".$bcData->client."','".$bcData->interface."',[".$bcData->roomids."], '".$bcData->version."'];if (bcData.loaded.indexOf('".$bcData->id."') == -1) {bcData.bcc.push(bcDataObj);bcData.loaded.push('".$bcData->id."');}</script>\n";
			}
		}
		if ($bcData->detached) {
			drupal_add_js("bcData.detached = 1;", array('type' => 'inline', 'scope' => 'header', 'weight' => 5));
			//$output .= "<script type=\"text/javascript\">bcData.detached = 1;</script>\n";
		}
			
		$called = true;
		
		return $output;
	}
	
	protected function bc_getTheme($par) {
		$theme = new stdClass();
		$theme->name = $par["theme"];
		$theme->cloc = "";
		$theme->over = "";
		if ($par["themecname"] != "" && $par["themecloc"] != "") {
			$theme->name = $par["themecname"];
			$theme->cloc = $par["themecloc"];
		}
		$theme->over = $par["themeover"];
		return $theme;
	}
	*/
	
	/*
	return language file name in the "languages" subdirectory, we will prepend .js extension to it
	*/
	/*
	protected function bc_getLanguageFile($user, $par) {
		$lang = "en";
		if ($par["lang"]) {
			$lang = $par["lang"];
		}
		return $lang;
	}
	*/
	
	/*
	return predefined guest names, if empty generated guest name will be used
	*/
	/*
	protected function bc_getGuestNames() {
		//return array("John", "Peter", "Mark", "Jim", "Bill");
		return array();
	}
	*/
	
	/*
	return user object (containing at least id, username, name)
	$user->id
	$user->username
	$user->name
	*/
	/*
	protected function bc_getUser() {
		global $user;
		$loaded_user = user_load($user->uid);

		//we could call instead
		//$loadeduser = user_load($user->uid);
		//and then transfer data from $loadeduser to $myuser
		
		$myuser = new stdClass();
		$myuser->id = $loaded_user->uid;
		$myuser->username = $loaded_user->name;
		$myuser->name = $loaded_user->name;
		return $myuser;
	}
	*/
	
	/*
	return comma separated string of user group ids
	*/
	/*
	protected function bc_getUserGroups($user) {
		//find gids
		$loaded_user = user_load($user->id);
		$roles = [];
		foreach($loaded_user->roles as $key => $value) {
			$roles[] = $key;
		}
		$gids = implode(",", $roles); //drupal roles ( 1-anonymous user, 2-authenticated user, 3- administrator )       
		return $gids;
	}
	*/
	
	/*
	returns int
	0 - unknown, 1 - male, 2 - female
	*/
	/*
	protected function bc_getUserGender($user) {
		//Drupal does not save information about gender
		//$user->gender
		$gender = 0;
		return $gender;
	}
	*/
	
	/*
	return comma separated string of user friends IDs
	*/
	/*
	protected function bc_getUserFriends($user) {
		//$friends = implode(',', array('1','2','3'));
		$friends = "";
		return $friends;
	}
	*/
	
	/*
	protected function bc_getUserAvatar($user, $type) {
		$avt_file = "";
		$avt_commonpath = "";
		$avt_path = "";
		$row = null;

		$avatar = new stdClass();
		
		if ($type == 1) {
			$user_item = user_load($user->id);
			//theme('user_picture', array('account' =>$user_item));
			$avt_file = $user_item->picture;
		} else if ($type == 2) {
			//custom
		}
		$avatar->commonpath = $avt_commonpath;
		$avatar->file = $avt_file;
		return $avatar;
	}
	*/
	
	/*
	protected function bc_getVersion() {
		//prepare variables dependent on system used
		// $_VERSION - variable holding CMS information
		// $_VERSION->PRODUCT - product used
		// $_VERSION->RELEASE - release number of product used
		// $_VERSION->DEV_LEVEL  - development number of product used
		if (!defined("VERSION")) {
			$rel = "";
			$dev = "";
		} else {
			$rel = explode('.',VERSION);
			$dev = $rel[1];
			$rel = $rel[0];
		}
		$bc_version = new stdClass();
		$bc_version->PRODUCT = 'Drupal';
		$bc_version->RELEASE = $rel;
		$bc_version->DEV_LEVEL = $dev;	
		return $bc_version;
	}
	*/
}