/* do not remove first line */
if (typeof bcDataConfig === 'undefined') {bcDataConfig = {"system": {},"language": {},"groups": [],"rooms": [], "groupsrooms": [], "emoticons": {"emoticons": [], "groups": []},"sounds": {"sounds": [], "groups": []}};}

/* system wide configuration */
bcDataConfig.system.mainTitle = "Our Chat";
bcDataConfig.system.sessionKeepAlive = 14;
bcDataConfig.system.sessionUrl = "blastchat?bc_task=keepsession";
bcDataConfig.system.detachUrl = "blastchat";
//additional parameters here